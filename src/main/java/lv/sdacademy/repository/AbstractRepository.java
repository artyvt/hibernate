package lv.sdacademy.repository;

import lv.sdacademy.utils.HibernateUtils;
import lv.sdacademy.utils.PageRequest;

import javax.persistence.Query;
import java.util.List;

public class AbstractRepository<REQUEST, ID> implements Repository<REQUEST, ID> {

    @Override
    public REQUEST getById(Class<REQUEST> request, ID id) {
        return HibernateUtils.transactionWithResult(session -> session.find(request, id));
    }

    @Override
    public void create(REQUEST request) {
        HibernateUtils.transactionWithoutResults(session -> {
            session.save(request);
        });
    }

    @Override
    public void update(REQUEST request) {
        HibernateUtils.transactionWithoutResults(session -> {
            session.update(request);
        });
    }

    @Override
    public void delete(REQUEST request) {
        HibernateUtils.transactionWithoutResults(session -> {
            session.delete(request);
        });
    }

    @Override
    public List<REQUEST> findAll(Class<REQUEST> requestClass) {
        String className = requestClass.getSimpleName();
        return HibernateUtils.transactionWithResult(session -> {
            return session.createQuery("from " + className, requestClass).getResultList();
        });
    }

    @Override
    public List<REQUEST> findByPage(Class<REQUEST> requestClass, PageRequest pageRequest) {
        String className = "From " + requestClass.getSimpleName();

        return HibernateUtils.transactionWithResult(session -> {
            Query query = session.createQuery(className);
            query.setFirstResult(pageRequest.getPageNumber());
            query.setMaxResults(pageRequest.getPageSize());
            return query.getResultList();
        });
    }
}
