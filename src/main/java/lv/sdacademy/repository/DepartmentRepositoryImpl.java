package lv.sdacademy.repository;

import lv.sdacademy.domain.Department;
import lv.sdacademy.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import java.util.List;

public class DepartmentRepositoryImpl implements DepartmentRepository {

    @Override
    public Department getById(Integer id) {

        try (Session session = HibernateUtils.openSession();
        ) {
            Department department = session.find(Department.class, id);
            if (department == null) {
                throw new IllegalArgumentException("Department by id: " + id + " not found");
            }
            return department;
        }
    }

    @Override
    public void create(Department department) {
        try (Session session = HibernateUtils.openSession();
        ) {
            Transaction transaction = session.beginTransaction();
            if (department.getId() != null) {
                throw new IllegalArgumentException("Department already saved!");
            }
            session.save(department);
            transaction.commit();
        }
    }

    @Override
    public void update(Department department) {
        try (Session session = HibernateUtils.openSession();
        ) {
            Transaction transaction = session.beginTransaction();
            if (department.getId() == null) {
                throw new IllegalArgumentException("Department doesn't exist!");
            }
            session.update(department);
            transaction.commit();
        }
    }

    @Override
    public void delete(Department department) {
        try (Session session = HibernateUtils.openSession();
        ) {
            Transaction transaction = session.beginTransaction();
            if (department.getId() == null) {
                throw new IllegalArgumentException("Department doesn't exist!");
            }
            session.delete(department);
            transaction.commit();
        }
    }

    @Override
    public List<Department> findAll() {
        try (Session session = HibernateUtils.getSessionFactory().openSession();
        ) {
            String selectAll = "from Department";
            Query sessionQuery = session.createQuery(selectAll);
            List<Department> allDepartments = sessionQuery.getResultList();
            return allDepartments;
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Can't connect to DB ", e);
        }
    }
}
