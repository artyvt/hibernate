package lv.sdacademy.repository;

import lv.sdacademy.domain.Department;

import java.util.List;

public interface DepartmentRepository {
    Department getById(Integer id);

    void create(Department department);

    void update(Department department);

    void delete(Department department);

    List<Department> findAll();
}
