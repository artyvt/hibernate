package lv.sdacademy.repository;

import lv.sdacademy.domain.Department;
import lv.sdacademy.domain.Employee;
import lv.sdacademy.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import java.nio.file.SecureDirectoryStream;
import java.util.List;

public class EmployeeRepositoryImpl implements EmployeeRepository {
    @Override
    public Employee getById(Integer id) {
        try (Session session = HibernateUtils.openSession()) {
            Employee employee = session.find(Employee.class, id);
            if (employee == null) {
                throw new IllegalArgumentException("Employee with id: " + id + " doesn't exist!");
            }
            return employee;
        }
    }

    @Override
    public void create(Employee employee) {
        try (Session session = HibernateUtils.openSession()) {
            if (employee.getId() != null) {
                throw new IllegalArgumentException("The employee already exist!");
            }
            Transaction transaction = session.beginTransaction();
            session.save(employee);
            transaction.commit();
        }
    }

    @Override
    public void update(Employee employee) {
        try (Session session = HibernateUtils.openSession()) {
            if (employee.getId() == null) {
                throw new IllegalArgumentException("Can't update employee, " +
                        "as one with id: " + employee.getId() + " doesn't exist");
            }
            Transaction transaction = session.beginTransaction();
            session.update(employee);
        }

    }

    @Override
    public void delete(Employee employee) {
        try (Session session = HibernateUtils.openSession()) {
            if (employee.getId() == null) {
                throw new IllegalArgumentException("No employee with id: " +
                        employee.getId() + ", Nothing to delete");
            }
            Transaction transaction = session.beginTransaction();
            session.delete(employee);
        }
    }

    @Override
    public List<Employee> findAll() {
        try (Session session = HibernateUtils.getSessionFactory().openSession();
        ) {
            String selectAll = "from Employee";
            Query sessionQuery = session.createQuery(selectAll);
            List<Employee> employeesList = sessionQuery.getResultList();
            return employeesList;
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Can't connect to Employee DB ", e);
        }
    }
}

