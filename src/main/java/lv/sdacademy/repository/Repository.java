package lv.sdacademy.repository;

import lv.sdacademy.utils.PageRequest;

import java.util.List;

public interface Repository<REQUEST, ID> {
    REQUEST getById(Class<REQUEST> request, ID id);

    void create(REQUEST request);

    void update(REQUEST request);

    void delete(REQUEST request);

    List<REQUEST> findAll(Class<REQUEST> requestClass);

    List<REQUEST> findByPage(Class<REQUEST> requestClass, PageRequest pageRequest);
}
