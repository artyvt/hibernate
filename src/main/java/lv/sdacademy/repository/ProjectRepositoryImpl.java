package lv.sdacademy.repository;

import lv.sdacademy.domain.Project;
import lv.sdacademy.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import java.util.List;

public class ProjectRepositoryImpl implements ProjectRepository {
    @Override
    public Project getById(Integer id) {
        try (Session session = HibernateUtils.openSession()) {
            Project project = session.find(Project.class, id);
            if (project == null) {
                throw new IllegalArgumentException("Project with id: " + id + " not found!");
            }
            return project;
        }
    }

    @Override
    public void create(Project project) {
        try (Session session = HibernateUtils.openSession()) {
            Transaction transaction = session.beginTransaction();
            if (project.getId() != null) {
                throw new IllegalArgumentException("Project with id: " + project.getId() + " already exist!");
            }
            session.save(project);
            transaction.commit();
        }
    }

    @Override
    public void update(Project project) {
        try (Session session = HibernateUtils.openSession()) {
            Transaction transaction = session.beginTransaction();
            if (project.getId() == null) {
                throw new IllegalArgumentException("Project doesn't exist, can't update");
            }
            session.update(project);
            transaction.commit();
        }

    }

    @Override
    public void delete(Project project) {
        try (Session session = HibernateUtils.openSession()) {
            Transaction transaction = session.beginTransaction();
            if (project.getId() == null) {
                throw new IllegalArgumentException("Project doesn't exist, cant delete");
            }
            session.delete(project);
            transaction.commit();
        }

    }

    @Override
    public List<Project> findAll() {
        try (Session session = HibernateUtils.getSessionFactory().openSession()) {
            String selectProject = "from Project";
            Query selectProjectQuery = session.createQuery(selectProject);
            List<Project> projectList = selectProjectQuery.getResultList();
            return projectList;
        }catch (IllegalArgumentException exception){
            throw new IllegalArgumentException("Can't connect to Project DB", exception);
        }
    }
}
