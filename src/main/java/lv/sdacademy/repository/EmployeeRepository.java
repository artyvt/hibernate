package lv.sdacademy.repository;

import lv.sdacademy.domain.Employee;

import java.util.List;

public interface EmployeeRepository {
    Employee getById(Integer id);

    void create(Employee employee);

    void update(Employee employee);

    void delete(Employee employee);

    List<Employee> findAll();
}
