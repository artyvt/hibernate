package lv.sdacademy.repository;

import lv.sdacademy.domain.Project;

import java.util.List;

public interface ProjectRepository {
    Project getById(Integer id);

    void create(Project project);

    void update(Project project);

    void delete(Project project);

    List<Project> findAll();
}
