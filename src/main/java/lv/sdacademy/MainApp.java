package lv.sdacademy;

import lv.sdacademy.domain.Department;
import lv.sdacademy.domain.Employee;
import lv.sdacademy.domain.Project;
import lv.sdacademy.repository.*;
import lv.sdacademy.utils.PageRequest;

import java.util.List;

public class MainApp {
    public static void main(String[] args) {
        System.out.println("####################################################################");


        AbstractRepository repository = new AbstractRepository();
        Object department = repository.getById(Department.class, 2);
        System.out.println(department.toString());

        System.out.println("####################################################################");

        List<Employee> projectList = repository.findAll(Employee.class);
        projectList.forEach(project -> {
            System.out.println(project.toString());
        });
        System.out.println("####################################################################");

        PageRequest pageRequest = new PageRequest(0, 2);
        List<Employee> employeeList = repository.findByPage(Employee.class, pageRequest);
        employeeList.forEach(employee -> {
            System.out.println(employee.toString());
        });
    }
}
