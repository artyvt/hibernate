package lv.sdacademy.utils;

import lv.sdacademy.domain.Department;
import lv.sdacademy.domain.Employee;
import lv.sdacademy.domain.Project;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class HibernateUtils {
    public static final String DATABASE_HOST = "jdbc:mysql://localhost:3306/jdbc_course_javaRiga10?serverTimezone=UTC";
    public static final String DATABASE_USERNAME = "root";
    public static final String DATABASE_PASSWORD = "A0B1C2_ZZ_$aatttfff"; //change to your password


    public static SessionFactory sessionFactory;

    public static Session openSession() {
        return getSessionFactory().openSession();
    }

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration conf = new Configuration();

                Properties settings = new Properties();
                settings.put(AvailableSettings.DRIVER, "com.mysql.jdbc.Driver");
                settings.put(AvailableSettings.URL, DATABASE_HOST);
                settings.put(AvailableSettings.USER, DATABASE_USERNAME);
                settings.put(AvailableSettings.PASS, DATABASE_PASSWORD);
                settings.put(AvailableSettings.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
                settings.put(AvailableSettings.SHOW_SQL, "true");
                settings.put(AvailableSettings.HBM2DDL_AUTO, "validate");

                conf.setProperties(settings);
                conf.addAnnotatedClass(Department.class);
                conf.addAnnotatedClass(Employee.class);
                conf.addAnnotatedClass(Project.class);

                ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                        .applySettings(conf.getProperties())
                        .build();

                sessionFactory = conf.buildSessionFactory(serviceRegistry);
            } catch (Exception ex) {
                throw new RuntimeException("Error creating session factory", ex);
            }
        }
        return sessionFactory;
    }


    public static <REQUEST> REQUEST transactionWithResult(dbAction<REQUEST> dbAction) {
        Session session = getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            REQUEST request = dbAction.execute(session);
            transaction.commit();
            return request;
        } catch (Exception exception) {
            exception.printStackTrace();
            transaction.rollback();
            throw exception;
        } finally {
            session.close();
        }
    }

    public static void transactionWithoutResults(dbActionWithoutResult dbActionWithoutResult) {
        Session session = getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        try {
            dbActionWithoutResult.execute(session);
            transaction.commit();
        } catch (Exception exception) {
            exception.printStackTrace();
            throw exception;
        }finally {
            session.close();
        }
    }

    public interface dbAction<REQUEST> {
        public REQUEST execute(Session session);
    }

    public interface dbActionWithoutResult<REQUEST> {
        public void execute(Session session);
    }
}
